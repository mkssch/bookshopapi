## Quickstart
1. [Install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. [Install docker](https://docs.docker.com/install/)
3. [Install docker-compose](https://docs.docker.com/compose/install/)
4. Run
```
git clone https://mkssch@bitbucket.org/mkssch/bookshopapi.git
cd bookshopapi
cp docker/.env.docker .env
docker-compose up -d --build

docker-compose run --rm php-cli composer install
```
Go to [http://localhost:8081](http://localhost:8081)