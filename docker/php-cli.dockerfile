FROM php:7.2-cli

RUN apt-get update && apt-get install -y zlib1g-dev zip libpq-dev

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql

RUN docker-php-ext-install zip pdo_pgsql

RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer --quiet

WORKDIR /app