<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930120143 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE books_authors_link_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE books_authors_link (id INT NOT NULL, book_id INT NOT NULL, author_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D54CBF3A16A2B381 ON books_authors_link (book_id)');
        $this->addSql('CREATE INDEX IDX_D54CBF3AF675F31B ON books_authors_link (author_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D54CBF3A16A2B381F675F31B ON books_authors_link (book_id, author_id)');
        $this->addSql('ALTER TABLE books_authors_link ADD CONSTRAINT FK_D54CBF3A16A2B381 FOREIGN KEY (book_id) REFERENCES books (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE books_authors_link ADD CONSTRAINT FK_D54CBF3AF675F31B FOREIGN KEY (author_id) REFERENCES authors (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE books_authors_link_id_seq CASCADE');
        $this->addSql('DROP TABLE books_authors_link');
    }
}
