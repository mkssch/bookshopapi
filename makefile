init: down-clear build up migrations

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down --remove-orphans

down-clear:
	docker-compose down -v --remove-orphans

test:
	docker-compose run --rm php-cli php bin/phpunit

migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate -n

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load