<?php

namespace App\UseCase\Order\Create;

use App\Validators\PhoneConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\NotBlank()
     * @Assert\Collection(fields={}, allowExtraFields=true)
     */
    public $books;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    public $address;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     * @PhoneConstraint()
     */
    public $phone;
}