<?php

namespace App\UseCase\Order\Create;

use App\Entity\Order;
use App\Repository\BooksRepository;
use App\Types\Order\BooksOrder;
use App\Types\Phone;
use App\Types\Status;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{

    /**
     * @var BooksRepository
     */
    private $booksRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(BooksRepository $booksRepository, EntityManagerInterface $manager)
    {
        $this->booksRepository = $booksRepository;
        $this->manager = $manager;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command)
    {
        $books = $this->booksRepository->findBy(['id' => $command->books]);
        $booksOrder = new BooksOrder();
        foreach ($books as $book){
            $booksOrder->addBook($book);
        }
        $order = new Order(
            Status::pending(),
            $command->address,
            new Phone($command->phone),
            $booksOrder,
            new \DateTimeImmutable('now')
        );
        $this->manager->persist($order);
        $this->manager->flush();
    }
}