<?php


namespace App\UseCase\Books\Create;


use App\Entity\Books;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function create(Command $book) : void
    {
        $book = new Books($book->title, $book->description);
        $this->manager->persist($book);
        $this->manager->flush();
    }
}