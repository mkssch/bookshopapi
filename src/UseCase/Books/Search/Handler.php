<?php


namespace App\UseCase\Books\Search;


use App\Repository\BooksRepository;

class Handler
{
    /**
     * @var BooksRepository
     */
    private $booksRepository;

    public function __construct(BooksRepository $booksRepository)
    {
        $this->booksRepository = $booksRepository;
    }

    public function search(Command $command): array
    {
        return $this->booksRepository->createQueryBuilder('b')
            ->where('b.title LIKE :title')
            ->getQuery()
            ->setParameter('title', "%{$command->title}%")
            ->getArrayResult();
    }
}