<?php


namespace App\UseCase\Books\Search;


use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="50")
     */
    public $title;
}