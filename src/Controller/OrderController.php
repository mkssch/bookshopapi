<?php

namespace App\Controller;

use App\UseCase\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/new", methods={"POST"})
     * @param Request $request
     * @param Order\Create\Handler $handler
     * @return Response
     */
    public function new(Request $request, Order\Create\Handler $handler): Response
    {
        $command = new Order\Create\Command();
        $form = $this->createForm(Order\Create\OrderType::class, $command);
        try {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $handler->handle($command);
                return $this->json([
                    'error' => false
                ]);
            }
        } catch (\Throwable $e) {
            return $this->json([
                'error' => true,
                'errors' => $e->getMessage(),
            ]);
        }
        return $this->json([
            'error' => true,
            'errors' => $form->getErrors(true),
        ]);
    }
}
