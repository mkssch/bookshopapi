<?php

namespace App\Controller;

use App\Repository\BooksRepository;
use App\UseCase\Books\Search\Command;
use App\UseCase\Books\Search\Handler;
use App\UseCase\Books\Search\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(BooksRepository $booksRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'books' => $booksRepository->findAll(),
        ]);
    }

    /**
     * @Route("/book-search", methods={"POST"})
     * @param Handler $handler
     * @param Request $request
     * @return Response
     */
    public function search(Handler $handler, Request $request): Response
    {
        $command = new Command();
        $form = $this->createForm(SearchType::class, $command);
        try {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()){
                return $this->json($handler->search($command));
            }
        } catch (\Throwable $e){
            return $this->json(['error' => true, 'errors' => $e->getMessage()]);
        }
        return $this->json(['error' => true, 'errors' => $form->getErrors()]);
    }
}
