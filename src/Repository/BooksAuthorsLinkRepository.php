<?php

namespace App\Repository;

use App\Entity\BooksAuthorsLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BooksAuthorsLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method BooksAuthorsLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method BooksAuthorsLink[]    findAll()
 * @method BooksAuthorsLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BooksAuthorsLinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BooksAuthorsLink::class);
    }

    // /**
    //  * @return BooksAuthorsLink[] Returns an array of BooksAuthorsLink objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BooksAuthorsLink
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
