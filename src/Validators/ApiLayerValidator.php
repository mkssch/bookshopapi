<?php


namespace App\Validators;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Webmozart\Assert\Assert;

class ApiLayerValidator extends ConstraintValidator
{
    /**
     * @var HttpClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $apiKey;

    public function __construct(HttpClientInterface $client, string $apiKey)
    {
        Assert::notEmpty($apiKey);
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function validate($value, Constraint $constraint)
    {
        Assert::isInstanceOf($constraint, PhoneConstraint::class);

        Assert::notEmpty($value);
        Assert::string($value);

        $response = $this->client->request(
            'GET',
            'http://apilayer.net/api/validate',
            [
                'query' => [
                    'access_key' => $this->apiKey,
                    'number' => preg_replace('/\D/', '', $value),
                    'format' => 1 // JSON response
                ]
            ]
        );

        $data = $response->toArray();
        if (!isset($data['valid'])) {
            $this->context->addViolation('Phone API validation error');
        }
        if (isset($data['valid']) && $data['valid'] === false) {
            /** @var PhoneConstraint $constraint */
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{[phone}}', $value)
                ->addViolation();
        }
    }
}