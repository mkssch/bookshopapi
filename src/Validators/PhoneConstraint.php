<?php


namespace App\Validators;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PhoneConstraint extends Constraint
{
    public $message = 'The phone: {{phone}} isn\'t valid!';

    public function validatedBy()
    {
        return ApiLayerValidator::class;
    }
}