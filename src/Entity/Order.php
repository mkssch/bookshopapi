<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Types\Order\BooksOrder;
use App\Types\Phone;
use App\Types\Status;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="books_order")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var Phone
     * @ORM\Column(type="phone_type", length=50, nullable=false)
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime_immutable", name="created_at", nullable=false)
     */
    private $createdAt;

    /**
     * @var Status
     * @ORM\Column(type="status_type", nullable=false)
     */
    private $status;

    /**
     * @var BooksOrder
     * @ORM\Column(type="books_order_type", nullable=false)
     */
    private $books;

    public function __construct(Status $status, string $address, Phone $phone, BooksOrder $books, \DateTimeImmutable $createdAt, int $id = null)
    {
        $this->id = $id;
        $this->status = $status;
        $this->address = $address;
        $this->phone = $phone;
        $this->books = $books;
        $this->createdAt = $createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getPhone(): int
    {
        return $this->phone->getPhone();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getBooks(): array
    {
        return $this->books->getBooksIds();
    }

    public function addBook(Books $book): void
    {
        $this->books->addBook($book);
    }

    public function isPending(): bool
    {
        return $this->status->isPending();
    }

    public function isProcessed(): bool
    {
        return $this->status->isProcessed();
    }

    public function isDelivered(): bool
    {
        return $this->status->isDelivered();
    }

    public function setProcessedStatus(): void
    {
        $this->status->setProcessedStatus();
    }

    public function setDeliveredStatus(): void
    {
        $this->status->setDeliveredStatus();
    }
}
