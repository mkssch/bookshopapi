<?php

namespace App\Entity;

use App\Repository\AuthorsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AuthorsRepository::class)
 */
class Authors
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=BooksAuthorsLink::class, mappedBy="author")
     */
    private $booksAuthorsLinks;

    public function __construct()
    {
        $this->booksAuthorsLinks = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|BooksAuthorsLink[]
     */
    public function getBooksLinks(): Collection
    {
        return $this->booksAuthorsLinks;
    }

    public function addBooksLink(BooksAuthorsLink $booksAuthorsLink): self
    {
        if (!$this->booksAuthorsLinks->contains($booksAuthorsLink)) {
            $this->booksAuthorsLinks[] = $booksAuthorsLink;
            $booksAuthorsLink->setAuthor($this);
        }

        return $this;
    }
}
