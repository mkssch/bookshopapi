<?php

namespace App\Entity;

use App\Repository\BooksRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BooksRepository::class)
 */
class Books
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=BooksAuthorsLink::class, mappedBy="book")
     */
    private $booksAuthorsLinks;

    public function __construct(string $title, string $description)
    {
        $this->booksAuthorsLinks = new ArrayCollection();
        $this->title = $title;
        $this->description = $description;
    }

    public static function createBookForTests(int $id = null, string $title = '', string $description = ''): self
    {
        $book = new self($title, $description);
        $book->id = $id;
        return $book;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|BooksAuthorsLink[]
     */
    public function getAuthorsLinks(): Collection
    {
        return $this->booksAuthorsLinks;
    }

    public function addAuthorsLink(BooksAuthorsLink $booksAuthorsLink): self
    {
        if (!$this->booksAuthorsLinks->contains($booksAuthorsLink)) {
            $this->booksAuthorsLinks[] = $booksAuthorsLink;
            $booksAuthorsLink->setBook($this);
        }

        return $this;
    }
}
