<?php

namespace App\Entity;

use App\Repository\BooksAuthorsLinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BooksAuthorsLinkRepository::class)
 * @ORM\Table(
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"book_id", "author_id"})
 *     })
 */
class BooksAuthorsLink
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Books::class, inversedBy="booksAuthorsLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $book;

    /**
     * @ORM\ManyToOne(targetEntity=Authors::class, inversedBy="booksAuthorsLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    public function __construct(Books $book, Authors $author)
    {
        $this->book = $book;
        $this->author = $author;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getBook(): Books
    {
        return $this->book;
    }

    public function getAuthor(): Authors
    {
        return $this->author;
    }

    public function setBook(Books $book)
    {
        $this->book = $book;
    }

    public function setAuthor(Authors $author)
    {
        $this->author = $author;
    }
}
