<?php

namespace App\DataFixtures;

use App\Entity\Books;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BookFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (range(1, 6) as $id){
            $book = new Books("Title #{$id}", 'Some description');
            $manager->persist($book);
        }
        $manager->flush();
    }
}
