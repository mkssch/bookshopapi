<?php

namespace App\DataFixtures;

use App\Entity\Authors;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach (range(1, 6) as $id){
            $author = new Authors();
            $author->setName("Author #{$id}");
            $manager->persist($author);
        }
        $manager->flush();
    }
}
