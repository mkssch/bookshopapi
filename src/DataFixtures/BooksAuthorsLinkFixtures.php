<?php


namespace App\DataFixtures;


use App\Entity\Authors;
use App\Entity\Books;
use App\Entity\BooksAuthorsLink;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BooksAuthorsLinkFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $uniq = [];
        foreach (range(0, 10) as $id) {
            /** @var Books $book */
            $book = $manager->find(Books::class, rand(1, 6));
            /** @var Authors $author */
            $author = $manager->find(Authors::class, rand(1, 6));
            if (in_array($book->getId().$author->getId(), $uniq)){
                continue;
            } else {
                $uniq[] = $book->getId().$author->getId();
                $link = new BooksAuthorsLink($book, $author);
                $manager->persist($link);
            }
        }
        $manager->flush();
    }
}