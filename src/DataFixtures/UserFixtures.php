<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $superUser = new User();
        $superUser->setEmail('admin@admin.com');
        $superUser->setPassword('$argon2id$v=19$m=65536,t=4,p=1$6FLtPk2MRQ5UoF6hKS5jSg$Uji4IZdxgcH6rTwS0qnJlORSmxQYVsTNTDnhIdv1Br4');
        $superUser->setRoles(['ROLE_ADMIN']);
        $manager->persist($superUser);
        $manager->flush();
    }
}
