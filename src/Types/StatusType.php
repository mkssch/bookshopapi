<?php

namespace App\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\SmallIntType;

class StatusType extends SmallIntType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var Status $value */
        return $value->getValue();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new Status($value);
    }

    public function getName()
    {
        return 'status_type';
    }
}