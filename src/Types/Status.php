<?php

namespace App\Types;

use Webmozart\Assert\Assert;

class Status
{
    private const STATUS_PENDING = 1;
    private const STATUS_PROCESSED = 2;
    private const STATUS_DELIVERED = 3;

    /**
     * @var int
     */
    private $status;

    public function __construct(int $status)
    {
        Assert::inArray($status, [self::STATUS_PENDING, self::STATUS_PROCESSED, self::STATUS_DELIVERED]);
        $this->status = $status;
    }

    public static function pending(): self
    {
        return new self(self::STATUS_PENDING);
    }

    public function getValue(): int
    {
        return $this->status;
    }

    public function setProcessedStatus(): void
    {
        Assert::true($this->isPending());
        $this->status = self::STATUS_PROCESSED;
    }

    public function setDeliveredStatus(): void
    {
        Assert::true($this->isProcessed());
        $this->status = self::STATUS_DELIVERED;
    }

    public function isPending(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function isProcessed(): bool
    {
        return $this->status === self::STATUS_PROCESSED;
    }

    public function isDelivered(): bool
    {
        return $this->status === self::STATUS_DELIVERED;
    }
}