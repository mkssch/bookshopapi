<?php

namespace App\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class PhoneType extends StringType
{

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var Phone $value */
        return $value->getPhone();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new Phone($value);
    }

    public function getName()
    {
        return 'phone_type';
    }
}