<?php

namespace App\Types;

use Webmozart\Assert\Assert;

class Phone
{
    /**
     * @var string
     */
    private $phone;

    public function __construct(string $phone)
    {
        Assert::notEmpty($phone);
        $this->phone = preg_replace('/\D/', '', $phone);
    }

    public function getPhone() : string
    {
        return $this->phone;
    }
}