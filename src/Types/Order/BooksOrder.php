<?php


namespace App\Types\Order;


use App\Entity\Books;
use Webmozart\Assert\Assert;

class BooksOrder
{
    /**
     * @var array
     */
    private $ids = [];

    public function addBook(Books $book): void
    {
        if (in_array($book->getId(), $this->ids)){
            throw new \InvalidArgumentException('Book already in basket');
        }
        $this->ids[] = $book->getId();
    }

    public function getBooksIds()
    {
        return $this->ids;
    }

    /**
     * @param array $ids
     */
    public function setBooksId(array $ids): void
    {
        Assert::notEmpty($ids);
        $this->ids = $ids;
    }
}