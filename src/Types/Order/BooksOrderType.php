<?php


namespace App\Types\Order;



use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class BooksOrderType extends JsonType
{
    public function getName()
    {
        return 'books_order_type';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var BooksOrder $value */
        return parent::convertToDatabaseValue($value->getBooksIds(), $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new BooksOrder(parent::convertToPHPValue($value, $platform));
    }
}