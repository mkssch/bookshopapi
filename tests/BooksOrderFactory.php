<?php

namespace App\Tests;

use App\Types\Order\BooksOrder;

class BooksOrderFactory
{
    public static function createBooksOrder(array $booksId): BooksOrder{
        $books = new BooksOrder();
        $books->setBooksId($booksId);
        return $books;
    }

}