<?php

namespace App\Tests;

use App\Entity\Books;
use App\Types\Order\BooksOrder;
use PHPUnit\Framework\TestCase;
use Webmozart\Assert\InvalidArgumentException;

class OrderTest extends TestCase
{
    public function testCreateOrder(): void
    {
        $order = OrderFactory::createOrder(null, null, null, BooksOrderFactory::createBooksOrder([1, 2]));

        parent::assertTrue($order->isPending());
        parent::assertCount(2, $order->getBooks());

        $order->addBook(Books::createBookForTests(3, 'test', 'test'));
        parent::assertCount(3, $order->getBooks());

        parent::assertTrue(is_integer($order->getPhone()));

        parent::assertFalse(empty($order->getAddress()));

        parent::assertTrue($order->getCreatedAt() instanceof \DateTimeImmutable);
    }

    public function testAddDuplicateBookException(){
        $order = OrderFactory::createOrder(null, null, null, BooksOrderFactory::createBooksOrder([1, 2]));
        parent::expectErrorMessage('Book already in basket');
        $order->addBook(Books::createBookForTests(2, 'test', 'test'));
    }

    public function testChangeStatusFromPendingToDeliveredException(){
        $order = OrderFactory::createOrder();
        parent::expectException(InvalidArgumentException::class);
        $order->setDeliveredStatus();
    }

    public function testOrderLifeCycle(){
        $order = OrderFactory::createOrder();
        $order->setProcessedStatus();
        parent::assertTrue($order->isProcessed());
        $order->setDeliveredStatus();
        parent::assertTrue($order->isDelivered());
    }

    public function testEmptyBooksOrderException(){
        parent::expectException(InvalidArgumentException::class);
        OrderFactory::createOrder(null, null, null, BooksOrderFactory::createBooksOrder([]));
    }
}
