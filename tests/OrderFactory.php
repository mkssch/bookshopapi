<?php

namespace App\Tests;

use App\Entity\Order;
use App\Types\Order\BooksOrder;
use App\Types\Phone;
use App\Types\Status;

class OrderFactory
{
    public static function createOrder(
        Status $status = null,
        string $address = null,
        Phone $phone = null,
        BooksOrder $booksOrder = null,
        \DateTimeImmutable $date = null
    ): Order
    {
        return new Order(
            $status ?? Status::pending(),
            $address ?? 'Minsk',
            $phone ?? new Phone('+375 (29) 111-11-11'),
            $booksOrder ?? BooksOrderFactory::createBooksOrder([1, 2]),
            $date ?? new \DateTimeImmutable('now')
        );
    }
}